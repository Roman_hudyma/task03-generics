package com.epam.binarytree;

public class Node<T extends Comparable<T>> {
    T data;
    public Node<T> left;
    public Node<T> right;

    public Node(T data){
        this.data = data;
    }
}
