package com.epam.priorityqueue;

import java.util.ArrayList;

public class Droid implements Comparable {
    String name;
    String model;

    public Droid(String name, String model) {
        this.name = name;
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int compareTo(Droid o) {
        int result = this.name.compareTo(o.name);

        if (result == 0) {
            result = this.model.compareTo(o.model);
        }
        return result;
    }

    @Override
    public String toString() {
        return "Droid: " + name;
    }

    public static void main(String[] args) {
        System.out.println("The Lowest Priority is given to Droid: ");
        ArrayList<Droid> arr = new ArrayList<Droid>();
        Droid s1 = new Droid("A", "B");
        Droid s2 = new Droid("B", "A");
        Droid s3 = new Droid("C", "B");
        Droid s4 = new Droid("D", "V");
        Droid s5 = new Droid("X", "Z");

        arr.add(s1);
        arr.add(s2);
        arr.add(s3);
        arr.add(s4);
        arr.add(s5);

        PriorityQueue<Droid> queue = new PriorityQueue<Droid>();

        queue.add(s1);
        queue.add(s2);
        queue.add(s3);
        queue.add(s4);
        queue.add(s5);

        System.out.println("Printing out queue:");
        queue.print();
        System.out.println("--------------------------------------------");

        System.out.println("Priority Search:");
        System.out.println(queue.prioritySearch());
        System.out.println("--------------------------------------------");


        System.out.println("Removing: ");
        System.out.println("This item was removed: " + queue.remove());
        System.out.println("This item was removed: " + queue.remove());
        System.out.println("This item was removed: " + queue.remove());
        System.out.println("--------------------------------------------");

        System.out.println("Priority Search:");
        System.out.println(queue.prioritySearch());
        System.out.println("--------------------------------------------");

        System.out.println("BUILD SUCCESSFUL");

    }


    public int compareTo(Object o) {
        return 0;
    }
}


