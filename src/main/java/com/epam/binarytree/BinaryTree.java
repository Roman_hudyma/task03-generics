package com.epam.binarytree;

import java.util.ArrayList;
import java.util.List;

public class BinaryTree<T extends Comparable<T>> {
    private Node root;

    public Node getRoot() {
        return this.root;
    }

    public void add(T data) {
        Node<T> newNode = new Node<T>(data);
        if (root == null) {
            root = newNode;
        } else {
            Node<T> tempNode = root;
            Node<T> prev = null;
            while (tempNode != null) {
                prev = tempNode;
                if (data.compareTo(tempNode.data) > 0) {
                    tempNode = tempNode.right;
                } else {
                    tempNode = tempNode.left;
                }
            }
            if (data.compareTo(prev.data) < 0) {
                prev.left = newNode;
            } else {
                prev.right = newNode;
            }

        }
    }


    public void traverseInOrder(Node<T> root, List<T> storageList) {
        if (root != null) {
            traverseInOrder(root.left, storageList);
            storageList.add(root.data);
            traverseInOrder(root.right, storageList);
        }
    }

    public void traversePreOrder(Node<T> root, List<T> storageList) {
        if (root != null) {
            storageList.add(root.data);
            traversePreOrder(root.left, storageList);
            traversePreOrder(root.right, storageList);
        }
    }

    public void traversePostOrder(Node<T> root, List<T> storageList) {
        if (root != null) {
            traversePostOrder(root.left, storageList);
            traversePostOrder(root.right, storageList);
            storageList.add(root.data);
        }
    }

    public void printList(List<T> list) {
        for (T item : list) {
            System.out.println(item);
        }
    }


    public static void main(String args[]) {
        BinaryTree<Integer> Tree = new BinaryTree<>();
        Tree.add(50);
        Tree.add(30);
        Tree.add(60);
        Tree.add(25);
        Tree.add(40);
        Tree.add(35);
        Tree.add(70);
        Tree.add(65);

        System.out.println("<- Inorder Traversal ->");
        List<Integer> inOrderList = new ArrayList<>();
        Tree.traverseInOrder(Tree.getRoot(), inOrderList);
        Tree.printList(inOrderList);

        System.out.println("<- Pre Traversal ->");
        List<Integer> preOrderList = new ArrayList<>();
        Tree.traversePreOrder(Tree.getRoot(), preOrderList);
        Tree.printList(preOrderList);


        System.out.println("<- Post Traversal ->");
        List<Integer> postOrderList = new ArrayList<>();
        Tree.traversePostOrder(Tree.getRoot(), postOrderList);
        Tree.printList(postOrderList);


    }
}
